# [0.2.3](https://github.com/oriontvv/pyaspeller/releases/tag/0.2.3) (2022-02-01)
* migrate to pyproject.toml
* add python3.10 support
* add mypy
* add flake8
* add poetry


# [0.2.0](https://github.com/oriontvv/pyaspeller/releases/tag/0.2.2) (2020-12-26)
* added methods spelled, spell_path to Speller class
* use default format `plain`
* rewrite unittests to pytest
* drop python2 support
* updated example


# 0.1.0 (2016-03-03)
* add python2.7 support
* add Word quick spell class
* testing and coverage with pytest
* testing with tox
* project health by landscape.io
* add requirements status, python versions badges in readme
* add restrictions link in readme
* add tests, increase coverage


# 0.0.5 (2015-10-04)
* wheel packaging


# 0.0.4 (2015-10-08)
* system script *pyaspeller* added
* packet classifiers updated
* alpha status specified
* contributors files added


# 0.0.3 (2015-09-31)
* uploaded to pypi.
* test coverage increased.
* Pretty YandexSpeller API
* Spell checking of strings with parameters.


# 0.0.2 (2015-09-23)
* testing automated
* coverage setup
* basic functionality. spell checking string
